import numpy as np


class UniformDistribution:
    def __init__(self, min, max):
        self.values = np.random.uniform(min, max, 100000)
        self.last_value = 0

    def next_value(self):
        value = self.values[self.last_value]
        self.last_value = (self.last_value+1) % 100000
        return value
