import numpy as np

from utils.Simulation import Simulation
from utils.ExponentialDistribution import ExponentialDistribution
from utils.UniformDistribution import UniformDistribution
from utils import DroppingFunctions

arrival_distribution = ExponentialDistribution(1)
service_distribution = UniformDistribution(0,1)

Simulation(DroppingFunctions.RemDroppingFunction(2, 5, 0.7), arrival_distribution,
           service_distribution).start("simulationResults/REM_uniform_1__0_1___2_5_07")
