
import numpy as np

from utils.Simulation import Simulation
from utils.GammaDistribution import GammaDistribution
from utils.ExponentialDistribution import ExponentialDistribution
from utils import DroppingFunctions

arrival_intensity = 1



arrival_distribution = ExponentialDistribution(arrival_intensity)
service_distribution = GammaDistribution(2,1)

Simulation(DroppingFunctions.RemDroppingFunction(2, 5, 0.7), arrival_distribution,
           service_distribution).start("simulationResults/REM_gamma_arrival1__gamma2_1__2_5_07")