import numpy as np

from utils.Simulation import Simulation
from utils.ExponentialDistribution import ExponentialDistribution
from utils import DroppingFunctions

arrival_intensity = 10
service_intensity = 20

arrival_distribution = ExponentialDistribution(arrival_intensity)
service_distribution = ExponentialDistribution(service_intensity)

Simulation(DroppingFunctions.RemDroppingFunction(10, 20, 0.7), arrival_distribution,
           service_distribution).start("simulationResults/REM_exponential_10_20__10_20_07")
