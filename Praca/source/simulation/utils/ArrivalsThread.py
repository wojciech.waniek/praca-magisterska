import threading
import time
import numpy as np


class ArrivalsThread(threading.Thread):

    def __init__(self, q, distribution):
        super(ArrivalsThread, self).__init__()
        self._stop_event = threading.Event()
        self.q = q
        self.distribution = distribution
        self.count = 0

    def stop(self):
        self._stop_event.set()

    def stopped(self):
        return self._stop_event.is_set()

    def run(self):

        last_fire = time.clock()

        while not self.stopped():
            #new_recod = [[time.clock()-self.start_time, self.q.qsize()]]
            waiting_time = self.distribution.next_value()
           # print(str(waiting_time)+",")
            while(last_fire+waiting_time > time.clock()):
                pass
                # time.sleep(0.001)

            #print(str((time.clock()-last_fire))+",")
            last_fire = time.clock()
            self.q.put()
            self.count += 1
