import numpy as np


class GammaDistribution:
    def __init__(self, shape, scale):
        self.values = np.random.gamma(shape, scale , 100000)
        self.last_value = 0

    def next_value(self):
        value = self.values[self.last_value]
        self.last_value = (self.last_value+1) % 100000
        return value
