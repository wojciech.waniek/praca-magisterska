import threading
import time
import numpy as np


class DeparturesThread(threading.Thread):

    def __init__(self, q, distribution):
        super(DeparturesThread, self).__init__()
        self._stop_event = threading.Event()
        self.distribution = distribution
        self.q = q

    def stop(self):
        self._stop_event.set()

    def stopped(self):
        return self._stop_event.is_set()

    def run(self):

        last_fire = time.clock()

        while not self.stopped():
            waiting_time = self.distribution.next_value()
            
            while(last_fire+waiting_time > time.clock()):
                pass
     

            last_fire = time.clock()
            self.q.try_get()
            #print(waiting_time)
            #print(-last_fire +time.clock() )
