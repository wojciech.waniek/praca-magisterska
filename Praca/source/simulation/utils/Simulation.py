import _thread
import time
import numpy as np

from .QueueWithHistory import QueueWithHistory
from .ArrivalsThread import ArrivalsThread
from .DeparturesThread import DeparturesThread

class Simulation:

    def __init__(self, dropping_function, arrival_distribution, service_distribution):
        self.q = QueueWithHistory(dropping_function)
        self.dropping_function = dropping_function
        self.arrival_distribution = arrival_distribution
        self.service_distribution = service_distribution

    def start(self, file_name ,save_to_file=True):

        arrivals_thread = ArrivalsThread(self.q, self.arrival_distribution)
        departures_thread = DeparturesThread(self.q, self.service_distribution)

        arrivals_thread.start()
        departures_thread.start()

        printed_count=-1
        while arrivals_thread.count < 50000:
            if((arrivals_thread.count % 20 == 0) & (printed_count != arrivals_thread.count)):
                printed_count=arrivals_thread.count
                print("arrival nr " + str(arrivals_thread.count))
            if(arrivals_thread.count > 50):
                self.q.start_recording()

        arrivals_thread.stop()
        departures_thread.stop()
        if(save_to_file):
            self.q.save_history(file_name)

        return self.q.queue_size_history
