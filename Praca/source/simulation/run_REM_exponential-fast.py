import numpy as np

from utils.Simulation import Simulation
from utils.ExponentialDistribution import ExponentialDistribution
from utils import DroppingFunctions

arrival_intensity = 90
service_intensity = 100


arrival_distribution = ExponentialDistribution(arrival_intensity)
service_distribution = ExponentialDistribution(service_intensity)

Simulation(DroppingFunctions.RemDroppingFunction(2, 5, 0.7), arrival_distribution,
           service_distribution).start("simulationResults/REM_exponential_90_100__2_5_07")
