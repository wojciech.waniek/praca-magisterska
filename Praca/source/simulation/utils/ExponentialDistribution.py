import numpy as np

class ExponentialDistribution:
    def __init__(self,intensity):
        self.values = np.random.exponential(1/intensity,100000)
        self.last_value=0
        
    def next_value(self):
        value= self.values[self.last_value]
        self.last_value=(self.last_value+1)%100000
        return value