import queue
import numpy as np
import time
import datetime
import pickle
import random
import collections

class QueueWithHistory:

    def __init__(self, dropping_function):
        self.q = collections.deque()
        self.dropping_function = dropping_function
        self.started=False
        self.is_in_stationary_state = False
        self.random_values = np.random.uniform(0, 1, 100000)
        self.last_random_value = 0

    def decision(self, probability):
        value = self.random_values[self.last_random_value]
        self.last_random_value = (self.last_random_value+1) % 100000
        return value < probability

    def start_recording(self):
        if(not self.started):
            self.started=True
            #print("start")
            self.queue_size_history = np.array([[0, len(self.q)]])
            self.queue_acceptance_history = np.array([[0,1]])
            self.start_time = time.clock()
            self.is_in_stationary_state = True

    def drop(self):
        return self.decision(self.dropping_function.get_drop_probability(len(self.q)))

    def put(self):

        if(self.drop()):
            self.update_acceptance_history(0)
            return
        self.update_acceptance_history(1)

        self.q.append("task")
        self.update_history()

    def try_get(self):
        if( len(self.q)==0):
         #   print("empty")
            return

        #print("get")
        self.q.popleft()
        self.update_history()

    def update_history(self):
        if(self.is_in_stationary_state):
            new_recod = [[time.clock()-self.start_time, len(self.q)]]
            self.queue_size_history = np.append(self.queue_size_history, new_recod, axis=0)

    def update_acceptance_history(self,accepted):
        if(self.is_in_stationary_state):
            new_recod = [[time.clock()-self.start_time, accepted]]
            self.queue_acceptance_history = np.append(self.queue_acceptance_history, new_recod, axis=0)

    def save_history(self, file_name):      
        with open(file_name + '_queue_size_history.pkl', 'wb') as output:
            pickle.dump(self.queue_size_history, output, pickle.HIGHEST_PROTOCOL)
        with open(file_name + '_queue_acceptance_history.pkl', 'wb') as output:
            pickle.dump(self.queue_acceptance_history, output, pickle.HIGHEST_PROTOCOL)
