import numpy as np


class RemDroppingFunction:

    def __init__(self, b0, b, p0):
        self.b0 = b0
        self.b = b
        self.p0 = p0

    def get_drop_probability(self, x):
        if(x <= self.b0):
            return 0
        if(x >= self.b):
            return 1

        return (self.p0/(np.exp(-self.b+1+self.b0)-1)) * np.exp(-x + self.b0) - self.p0/(np.exp(-self.b+1+self.b0)-1)


class RedDroppingFunction:

    def __init__(self, b0, b, p0):
        self.b0 = b0
        self.b = b
        self.p0 = p0

    def get_drop_probability(self, x):
        if(x <= self.b0):
            return 0
        if(x >= self.b):
            return 1

        return (self.p0/(self.b-self.b0-1))*x - self.p0*self.b0/(self.b-self.b0-1)


class GredDroppingFunction:

    def __init__(self, a, b, c, p1, p2):
        self.a = a
        self.b = b
        self.c = c
        self.p1 = p1
        self.p2 = p2

    def get_drop_probability(self, x):
        if(x < self.a):
            return 0

        if((x >= self.a) & (x < self.b)):
            return ((0-self.p1)/(self.a-self.b))*x+(0-((0-self.p1)/(self.a-self.b))*self.a)

        if((x >= self.b) & (x < self.c)):
            return ((self.p1-self.p2)/(self.b-self.c))*x+(self.p1-((self.p1-self.p2)/(self.b-self.c))*self.b)

        if(x >= self.c):
            return 1
